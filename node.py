#!/usr/bin/env python



def key_belongs_to(key, id_a, id_b):
    if key < id_a:
        return id_a
    elif key > id_b:
        return id_b
    elif key < (id_a + id_b)/2:
        return id_a
    else:
        return id_b

if __name__ == '__main__':
    n = Node("localhost", 8080)
    m = Node("123.123.123.123", 5000)
    print n.ID
    print n.fingertable.table
    n.fingertable.add_entry(m)
    print m.fingertable.table
