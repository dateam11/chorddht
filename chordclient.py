#!/usr/bin/env python

# controller.py sets up a node and couples it with a network connection
# and makes it available. It controls messages coming in and going out from
# it's node, and performs actions based on the messages received.
# This module defines chord protocol methods like successor, lookup .etc

from twisted.internet import reactor, protocol
from threading import Thread
import hashfactory
import math
import pickle
import sys

theIP = "localhost"
theNode = None

# How many nodes will the key ring scale to
SCALE = 128


# Messages Documentation
# Current Messages:
# To call a function on a target client
# ('CALL', '<func_name>', [<func_args>], ...
#                     ... (<caller_ip>, <caller_port>, <caller_id>))
#
# Result of a CALL query
# ('RESULT', --result--)
# Different types of results:
#            Returning the result of a successor(<for_key>) call:
#            --('SUCCESSOR', <for_key>, (<s_ip>, <s_port>, <s_id>))--
#            Returning the result of a predecessor() call:
#            --('PREDECESSOR', (<p_ip>, <p_port>, <p_id>))--
#
# To tell a target client you (p) are it's predecessor
# ('I_AM_PREDECESSOR', (<p_ip>, <p_port>, <p_id>))


class ChordProtocol(protocol.Protocol):
    """Handles the network control part of the chord dht protocol."""

    def __init__(self, factory):
        self.factory = factory

    def dataReceived(self, data):
        query = eval(data)
        if query and query[0] == 'CLOSE':
            self.transport.loseConnection()
        process(self.transport, query)
        print theNode.ring_repr()

    def connectionMade(self):
        self.factory.numProtocols += 1
        if self.factory.query:
            self.transport.write(str(self.factory.query))

    def connectionLost(self, reason):
        self.factory.numProtocols -= 1
        self.transport.write("Connection lost.")


class ChordProtocolFactory(protocol.ClientFactory):
    """Responsible to build a ChordProtocol."""

    def __init__(self, query=None):
        self.numProtocols = 0
        self.query = query

    # def startedConnecting(self, connector):
    #     print "Started to connect."

    def buildProtocol(self, address):
        return ChordProtocol(self)

    def clientConnectionLost(self, connector, reason):
        print 'Lost connection.  Reason:', reason

    def clientConnectionFailed(self, connector, reason):
        print 'Connection failed. Reason:', reason


def process(transport, query):
    print theNode.query_repr(), " => Received Query: ", query
    if query:
        if query[0] == 'CALL':
            caller = None
            try:
                caller = query[3]
            except IndexError:
                # No caller for this call
                # print "No caller for this call."
                pass
            result = do_method(query[1], query[2], caller=caller)
            result_tup = ('RESULT', result)
            # result_serial = pickle.dumps(result_tup)
            # Reply with the result
            if caller:
                # If caller is specified, forward call to that caller
                send_query(caller[0], caller[1], result_tup)
            else:
                # Simply reply on the same connection
                transport.write(str(result_tup))

        elif query[0] == 'RESULT':
            # print "Result received: ", query
            result = query[1]
            if result[0] == 'FORWARD':
                transport.loseConnection()
                # Possibly close the connection
            if result[0] == 'SUCCESSOR':
                if result[1] == 'STORE':
                    # Store the value in the dictionary on this node
                    theNode.hashtab[str(result[2])] = result[3]
                    print "------------------------------"
                    print theNode.query_repr(), "===>", theNode.hashtab
                    print "------------------------------"
                    
                if result[1] != 'STORE':
                    # The successor result belongs to the node wanting to
                    # know its successor
                    theNode.successor_node = result[2]
                    # Let the new successor know this too
                    return_query = ('I_AM_PREDECESSOR', theNode.query_repr())
                    send_query(result[2][0], result[2][1], return_query)
                    stabilise()

            if result[0] == 'PREDECESSOR':
                # This result usually comes from my successor
                # So I should become my successor's predecessor if I am not
                if result[1] == ():
                    return_query = ('I_AM_PREDECESSOR', theNode.query_repr())
                    send_query(result[1][0], result[1][1], return_query)

                elif not (theNode.IP == result[1][0] and
                          theNode.port == result[1][1]):
                    theNode.successor_node = result[1]
                    # Tell new successor about me being the predecessor
                    return_query = ('I_AM_PREDECESSOR', theNode.query_repr())
                    send_query(result[1][0], result[1][1], return_query)
                else:
                    # print "All Cool, I am the predecessor as always."
                    pass

        elif query[0] == 'I_AM_PREDECESSOR':
            theNode.predecessor_node = query[1]
            stabilise()
            # TODO: Close the connection
            # transport.loseConnection()
            succ = theNode.successor_node
            send_query(succ[0], succ[1], ('STABILISE', theNode.query_repr()))

        elif query[0] == 'STABILISE':
            stabilise()

class Node:
    """Implementation of a chord Node."""

    def __init__(self, ip="", port=""):
        self.IP = ip
        self.port = port
        self.ID = None
        self.fingertable = None
        self.hashtab = {}   # The actual storage
        self.successor_node = []
        self.predecessor_node = []

    def __eq__(self, other):
        """Condition for equality."""
        if other.key == self.key and other.port == self.port:
            return True
        return False

    def __repr__(self):
        return "<Node: [%d, %s:%d]>" % (self.ID, self.IP, self.port)

    def query_repr(self):
        return (self.IP, self.port, self.ID)

    def ring_repr(self):
        return "Pred<%s> --- Node<%s> --- Succ<%s>" % (self.predecessor_node,
                                                       self.query_repr(),
                                                       self.successor_node)

    def _hash(self, string):
        return hashfactory.generate_hash(string, SCALE)

    def successor_finger(self, key):
        """Get the node closest to key by moving through the fingertables.

        When 'node' A services a request to find the successor of the 'key',
        it first determines whether its own successor is the owner of k
        (the successor is simply entry 0 in the finger table). If it is,
        then A returns its successor in response to the
        request. Otherwise, node A finds node B in its finger table such
        that B has the largest hash smaller than the 'key', and forwards
        the request to B.
        """
        print "Finding the node for key: ", key
        prev_entry = self.fingertable.table[0]   # Pointer to previous entry
        for entry in self.fingertable.table:
            if entry['id'] >= key:
                break
            prev_entry = entry
        return prev_entry['succ'].query_repr()

    def receive_node_joined(self, node):
        """Receive a notification that some node has joined the network."""
        if node.ID == self.ID:
            return

        # Check if atm the successor node is not self
        if self.successor_node.ID != self.ID:
            # Checking if we can move further in the logical ring of nodes
            if self.successor_node.ID < node.ID:
                pass
        # Check finger table to find the node, if it's there
        for entry in self.fingertable:
            if entry['id'] <= node.ID:
                entry['succ'] = node


class FingerTable:
    """Implementation of a node's FingerTable."""

    def __init__(self, node):
        self.size = int(math.log(SCALE, 2))
        self.table = []
        self.ID = node.ID   # The node id/key
        self.node = node

    def build_init_table(self):
        """Initialize and build the finger table with self.size entries

        Entry i in the finger table of node n (ID) is the first node that
        succeeds or equals (n + 2^i).
        """
        for i in range(self.size):
            # Default successor is the node itself
            next_id = (self.ID + 2 ** i) % SCALE
            entry = {'id': next_id, 'succ': self.node}
            self.table.append(entry)

    def add_entry(self, node):
        """Update the finger table when a new node has joined."""
        # Run through the finger table until the entry with key greater than
        # node.key
        # print "Finding a place for node: ", node
        iter_entry = {}
        for entry in self.table:
            iter_entry = entry
            if entry['id'] >= node.ID:
                break
        # iter_entry now points to either the entry >= node or last entry
        # either ways, update iter_entry's succ to 'node'
        iter_entry['succ'] = node


##########################################################################
# Helper functions                                                       #
##########################################################################

def belongs_to_successor(i, a, b):
    """Check if key 'i' belongs to 'a''s successor 'b' or 'a'."""
    if i > a and i < b:
        if i >= (a + b)/2:
            return True
        else:
            return False
    else:
        # i is either > b or < a
        if ring_distance(i, b) <= ring_distance(i, a):
            return True
        else:
            return False


def ring_distance(a, b):
    """Given the 'SCALE' calculate absolute ring distance between
    points a and b"""
    a_origin = a
    b_origin = b
    if a > (SCALE/2):
        a_origin = SCALE - a
    if b > (SCALE/2):
        b_origin = SCALE - b
    return a_origin + b_origin


##########################################################################
# Chord implementation functions                                         #
##########################################################################

def successor_key(hashkey, value, caller=None):
    global theNode
    succ = theNode.successor_node
    if succ[2] == theNode.ID:
        # theNode is the only node in the ring
        # Return Message: ('SUCCESSOR', key, [--details--])
        message = ('SUCCESSOR', 'STORE', hashkey, value, theNode.query_repr())
        return message
    if hashkey > succ[2]:
        if belongs_to_successor(hashkey, theNode.ID, succ[2]):
            # Forward the successor call to my successor
            message = ('CALL', 'successor_key', [hashkey, value], caller)
            send_query(succ[0], succ[1], message)
            return ('FORWARD', succ)
        else:
            # Key belongs to me
            message = ('SUCCESSOR', 'STORE', hashkey, value, succ)
            return message
    else:
        message = ('SUCCESSOR', 'STORE', hashkey, value, succ)
        return message


def successor(key, caller=None):
    global theNode
    succ = theNode.successor_node
    if succ[2] == theNode.ID:
        # theNode is the only node in the ring
        # Return Message: ('SUCCESSOR', key, [--details--])
        message = ('SUCCESSOR', key, theNode.query_repr())
        return message
    if key > succ[2]:
        if belongs_to_successor(key, theNode.ID, succ[2]):
            # Forward the successor call to my successor
            message = ('CALL', 'successor', [key], caller)
            send_query(succ[0], succ[1], message)
            return ('FORWARD', succ)
        else:
            # Key belongs to me
            message = ('SUCCESSOR', key, succ)
            return message
    else:
        message = ('SUCCESSOR', key, succ)
        return message    


def predecessor():
    global theNode
    if theNode.predecessor_node != []:
        return ('PREDECESSOR', theNode.predecessor_node)
    return ('PREDECESSOR', ())


def get_root_node():
    """For demo purposes, the root node is on localhost:8080"""
    root_node = Node()
    root_node.IP = "localhost"
    root_node.port = 8080
    root_node.ID = root_node._hash(str("%s:%d" % ("localhost", 8080)))
    root_node.fingertable = FingerTable(root_node)
    root_node.fingertable.build_init_table()
    root_node.successor_node = root_node.query_repr()
    return root_node


def notify(new_node):
    """Notify a node that a new node with [ip, port, id] has been added."""
    pass


def join_ring(root_node):
    """theNode want's to join the ring, find it's successor."""
    # Finding successor
    query = ('CALL', 'successor', [theNode.ID], theNode.query_repr())
    send_query(root_node[0], root_node[1], query)


def stabilise():
    """Stabilize the ring after a node joins

    For theNode, it's successor's predecessor has to be theNode. Similarly,
    it will notify it's successor that theNode is his predecessor.
    """
    # Ask theNode.successor_node for it's predeccesor and set the result as
    # theNode's successor
    succ = theNode.successor_node
    print theNode.query_repr(), " => Attempting to stabilise."
    send_query(succ[0], succ[1], ('CALL', 'predecessor', []))


##########################################################################
# Querying functions                                                     #
##########################################################################

def do_method(method_name, method_args, caller=None):
    """Call the 'method_name' on theNode."""
    functionToCall = globals()[method_name]
    result = None
    if caller:
        result = functionToCall(*method_args, caller=caller)
    else:
        result = functionToCall(*method_args)
    # print "RESULT: ", result
    return result


def send_query(target_ip, target_port, query):
    # print "Sending query: %s to (%s:%d)" % (query, target_ip,
    #                                         target_port)
    reactor.connectTCP(target_ip, target_port,
                       ChordProtocolFactory(str(query)))


def setup_node(ip, port):
    """Performs the init process of Node on theNode."""
    theNode.IP = ip
    theNode.port = port
    theNode.ID = theNode._hash(str("%s:%d" % (ip, port)))
    theNode.fingertable = FingerTable(theNode)
    theNode.fingertable.build_init_table()
    theNode.successor_node = theNode.query_repr()


def store(key, value):
    """Store the key, value in the dht"""
    global theNode
    # Hashing the key
    # Form the keyname by appending the client details as well
    keyname = "%s:%d:%d:%s" % (theNode.IP, theNode.port, theNode.ID, key)
    hashkey = theNode._hash(str(keyname))

    query = ('CALL', 'successor_key', [hashkey, value], theNode.query_repr())
    send_query(theNode.IP, theNode.port, query)


def main():
    global theNode

    ip = "localhost"
    port = 8080
    testNode = False
    if len(sys.argv) > 1:
        if sys.argv[1] == 'test':
            testNode = True
            port = 8090
        elif sys.argv[1] == 'test2':
            testNode = True
            # port = 8090
            port = 6666

    theNode = Node()
    setup_node(ip, port)
    print "Setup Node: ", theNode

    # print theNode.fingertable.table
    # Start the server thread
    try:
        reactor.listenTCP(theNode.port, ChordProtocolFactory())
    except:
        print "Starting only client mode."

    print "Ready to listen on ", theNode.port

    # testq = ('CALL', 'successor', [3])
    # send_query(theNode.IP, theNode.port, testq)

    if testNode:
        join_ring(get_root_node().query_repr())
        store("position", ('B', 'B'))
        store("wall", ['Y', 'Y'])

    store("position", ('A', 'A'))
    store("wall", ['X', 'X'])

    reactor.run()

main()
