from twisted.internet import protocol, reactor
import sys


class ChordProtocol(protocol.Protocol):
    """Handles the network control part of the chord dht protocol."""

    def __init__(self, factory):
        self.factory = factory

    def dataReceived(self, queryData):
        pass

    def connectionMade(self):
        self.factory.numProtocols += 1
        print "Connection Made. Number of Nodes: ", self.factory.numProtocols
        self.transport.write(str(self.factory.query))

    def connectionLost(self, reason):
        self.factory.numProtocols -= 1
        self.transport.write("Connection lost.")


class ChordProtocolFactory(protocol.ClientFactory):
    """Responsible to build a ChordProtocol."""

    def __init__(self, query=None):
        self.numProtocols = 0
        self.query = query

    def startedConnecting(self, connector):
        print "Started to connect."

    def buildProtocol(self, address):
        return ChordProtocol(self)

    def clientConnectionLost(self, connector, reason):
        print 'Lost connection.  Reason:', reason

    def clientConnectionFailed(self, connector, reason):
        print 'Connection failed. Reason:', reason


def message_node(target_ip, target_port, method_name):
    query = ('CALL', method_name)
    reactor.connectTCP(target_ip, target_port,
                       ChordProtocolFactory(query)


if __name__ == '__main__':
    server_mode = True
    if len(sys.argv) > 1:
        if sys.argv[1] == "client":
            server_mode = False

    testquery = (1, 2, 3)
    if server_mode:
        reactor.listenTCP(8007, ChordProtocolFactory())
        print "Ready to listen on 8007"

    if not server_mode:
        # Also start a client
        reactor.connectTCP("localhost", 8007, ChordProtocolFactory(testquery))

    reactor.run()
