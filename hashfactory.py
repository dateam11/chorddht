#!/usr/bin/env python

# hashfactory.py:
# ------------------------------------------------------------
# Defines hash utility functions which is used
# by the chord DHT

import hashlib



def generate_hash(string, scale):
    h = hashlib.sha1()
    h.update(string)
    return long(h.hexdigest(), 16) % scale


if __name__ == '__main__':
    print generate_hash("localhost")
