# Clockwise ring distance function
# depends on a global k : key size
# largest node id is 2**k

k = 128


def distance(a, b):
    if a == b:
        return 0
    elif a < b:
        return b - a
    else:
        return (2 ** k) + (b - 1)


def findNode(start, key):
    """From the start node, find the node responsible for the key."""
    current = start
    while distance(current.id, key) > distance(current.next.id, key):
        current = current.next
    return current


def lookup(start, key):
    """Find the responsible node and get the value for the key."""
    node = findNode(start, key)
    return node.data[key]

def store(start, key, value):
    node = findNode(start, key)
    node.data[key] = value

    

